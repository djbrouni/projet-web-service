DROP TABLE IF EXISTS reservation;
DROP TABLE IF EXISTS chambre;
DROP TABLE IF EXISTS utilisateur;
DROP TABLE IF EXISTS hotel;



-- HOTEL 
CREATE TABLE hotel (
    id INT AUTO_INCREMENT PRIMARY KEY ,
    nom VARCHAR(60) NOT NULL,
	nbtotalchambre INT,
	nbchambredispo INT
);

-- HOTEL => INSERTIONS
INSERT INTO hotel(id, nom, nbtotalchambre, nbchambredispo) VALUES (1,'hotelA',10,10);
INSERT INTO hotel(id, nom, nbtotalchambre, nbchambredispo) VALUES (2,'hotelB',5,3);
INSERT INTO hotel(id, nom, nbtotalchambre, nbchambredispo) VALUES (3,'hotelC',7,7);

-- UTILISATEUR 
CREATE TABLE utilisateur (
    id INT AUTO_INCREMENT PRIMARY KEY ,
    nom VARCHAR(30) NOT NULL,
	prenom VARCHAR(30) NOT NULL,
	email VARCHAR(60) UNIQUE NOT NULL,
	mdp VARCHAR(60) NOT NULL
);


-- UTILISATEUR => INSERTIONS
INSERT INTO utilisateur(id, nom, prenom, email,mdp) VALUES (1,'KELLER','Alexis','alexiskeller@gmail.com','motdepasse');
INSERT INTO utilisateur(id, nom, prenom, email,mdp) VALUES (2,'DJEBROUNI','Alexis','alexisdjebrouni@gmail.com','123456789');
INSERT INTO utilisateur(id, nom, prenom, email,mdp) VALUES (3,'MARTIN','Lutherking','lutherkingmartin@gmail.com','abcdefghi');
INSERT INTO utilisateur(id, nom, prenom, email,mdp) VALUES (4,'JENAIPASDIDEE','nothing','nothingjenaipasdidee@gmail.com','password');


-- CHAMBRE
CREATE TABLE chambre (
    id INT AUTO_INCREMENT PRIMARY KEY ,
	idhotel INT,
    nom VARCHAR(30) NOT NULL
);

-- CHAMBRE => ALTERATIONS
ALTER TABLE chambre ADD CONSTRAINT FK_chambre_hotel FOREIGN KEY (idhotel) REFERENCES hotel (id);


-- CHAMBRE => INSERTIONS 
INSERT INTO chambre (id, idhotel, nom) VALUES (1,1,'chambre1');
INSERT INTO chambre (id, idhotel, nom) VALUES (2,1,'chambre2');
INSERT INTO chambre (id, idhotel, nom) VALUES (3,1,'chambre3');
INSERT INTO chambre (id, idhotel, nom) VALUES (4,1,'chambre4');
INSERT INTO chambre (id, idhotel, nom) VALUES (5,1,'chambre5');
INSERT INTO chambre (id, idhotel, nom) VALUES (6,1,'chambre6');
INSERT INTO chambre (id, idhotel, nom) VALUES (7,1,'chambre7');
INSERT INTO chambre (id, idhotel, nom) VALUES (8,1,'chambre8');
INSERT INTO chambre (id, idhotel, nom) VALUES (9,1,'chambre9');
INSERT INTO chambre (id, idhotel, nom) VALUES (10,1,'chambre10');
INSERT INTO chambre (id, idhotel, nom) VALUES (11,2,'chambre1');
INSERT INTO chambre (id, idhotel, nom) VALUES (12,2,'chambre2');
INSERT INTO chambre (id, idhotel, nom) VALUES (13,2,'chambre3');
INSERT INTO chambre (id, idhotel, nom) VALUES (14,2,'chambre4');
INSERT INTO chambre (id, idhotel, nom) VALUES (15,2,'chambre5');
INSERT INTO chambre (id, idhotel, nom) VALUES (16,3,'chambre1');
INSERT INTO chambre (id, idhotel, nom) VALUES (17,3,'chambre2');
INSERT INTO chambre (id, idhotel, nom) VALUES (18,3,'chambre3');
INSERT INTO chambre (id, idhotel, nom) VALUES (19,3,'chambre4');
INSERT INTO chambre (id, idhotel, nom) VALUES (20,3,'chambre5');
INSERT INTO chambre (id, idhotel, nom) VALUES (21,3,'chambre6');
INSERT INTO chambre (id, idhotel, nom) VALUES (22,3,'chambre7');


-- RESERVATION
CREATE TABLE reservation (
    id INT AUTO_INCREMENT PRIMARY KEY ,
	datedebut DATE,
	datefin DATE,
	idutilisateur INT,
	idchambre INT,
	idhotel INT
);

-- RESERVATION => ALTERATIONS
ALTER TABLE reservation ADD CONSTRAINT FK_reservation_utilisateur FOREIGN KEY (idutilisateur) REFERENCES utilisateur (id);
ALTER TABLE reservation ADD CONSTRAINT FK_reservation_chambre FOREIGN KEY (idchambre) REFERENCES chambre (id);
ALTER TABLE reservation ADD CONSTRAINT FK_reservation_hotel FOREIGN KEY (idhotel) REFERENCES hotel (id);

-- RESERVATION => INSERTIONS

INSERT INTO reservation (id, datedebut, datefin, idutilisateur, idchambre, idhotel) VALUES (1, '2020-11-20', '2020-12-05', 1, 12, 2);
INSERT INTO reservation (id, datedebut, datefin, idutilisateur, idchambre, idhotel) VALUES (2, '2020-11-20', '2020-12-05', 1, 14, 2);



