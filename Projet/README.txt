+=================================================================== Read me =======================================================================+
|																																					|
|		Voici les différentes étapes pour correctement faire compiler notre projet WebService :														|
| 																																					|
|		Partie BDD : 																																|
|			1. Télécharger mySQL lancez le sur un terminal via la commande : mysql 																	|
|																																					|
|			2.Créer une Base de donnée nommée "bdd_Hotel" via mysql 																				|
|																																					|
|				CREATE DATABASE bdd_sdzee DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;														|
|																																					|
|			3. Créer un utilisateur nommé "java" ayant pour mdp : "pwd" ayant tous les droits sur la BDD "bdd_Hotel" via mysql: 					|
|																																					|
|				CREATE USER 'java'@'localhost' IDENTIFIED BY 'pwd'; 																				|
|				GRANT ALL ON bdd_Hotel.* TO 'java'@'localhost' IDENTIFIED BY 'pwd'; 																|
|																																					|
|			4.Quitter mysql avec la commande : exit 																								|
|																																					|
|			5.Exécuter le script bdd.sql qui se trouve dans le répertoire BD via cette commande sur le terminal: 									|
|																																					|
|				mysql -u java-p bdd_Hotel < [source]/bdd. sql 																						|
|																																					|
|			6.Si vous voulez effectuer des requetes sur la BDD pour divers tests, veillez à utiliser la base de donnée "bdd_Hotel" 					|
|			grâce à cette commande via mysql :																										|
|																																					|
|				use bdd_Hotel																														|
|																																					|
|		Configuration du projet sur eclipse :																										|
|																																					|
|			1.Le projet java "HotelRestLet" correspond à la partie du projet qui gère le Serveur REST.												|
|																																					|
|			2.Le projet java "WS_Hotel_Reservation" correspond à la partie du projet qui gère le Serveur SOAP. 										|
|																																					|
|			3.Le projet java "WS_Hotel_Reservation_Client" correspond à la partie du projet qui gère le client Serveur REST. 						|
|																																					|
|			4.Vous trouverez toutes les librairies à importer dans les divers projets dans le fichier "Libraries"									|
|																																					|
|			5.Compiler au moins les projets "HotelRestLet", "WS_Hotel_Reservation" avec JAVA 1.8 													|
|																																					|
|		Execution du projet :																														|
|																																					|
|			1.Lancer le serveur REST en exécutant le fichier "RestDistrib.java" dans le projet "HotelRestLet" 										|
|																																					|
|			2.Lancer le serveur SOAP en cliquant droit sur le fichier "HotelSOAP.java", puis Run As > Run on server 								|
|																																					|
|			3.Créer un nouveau Web Service Client (après avoir lancer les serveurs REST et SOAP) dans le projet "WS_Hotel_Reservation_Client" :		|
|																																					|
|				service definition : "http://localhost:8080/WS_Hotel_Reservation/services/HotelSOAP?wsdl"											|
|																																					|
|				configuration : 																													|	
|																																					|
|					server runtime : Tomcat v7.0 Server																								|
|																																					|	
|					Web Service runtime : Apache Axis2																								|
|																																					|
|					Client project : Hotel_Reservation_Client 																						|
|																																					|
|			4.Exécuter le fichier "Main.java" qui se trouve dans le projet "WS_Hotel_Reservation_Client", les interractions se feront dans la 		|
|			console du Main.java"																													|
|																																					|
|																																					|
|		Tester les fonctionnalité de notre projet & amusez vous !																					|
|																																					|
|																																					|
+=================================================================== Read me =======================================================================+
