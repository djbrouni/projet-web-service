package tps.ws.deployment;

import java.io.IOException;

import org.restlet.data.Form;
import org.restlet.resource.*;

public class HotelSOAP {
	
	private String baseURL = "http://localhost:8182";
	
	public String getAvailableRooms(String startDate, String endDate){
		// Create the client resource  
		ClientResource hotelResource = new ClientResource(baseURL+"/hotels");
		hotelResource.getReference().addQueryParameter("startDate", startDate);
		hotelResource.getReference().addQueryParameter("endDate", endDate);
		

		try {
			String result =  hotelResource.get().getText();
			hotelResource.release();
			return result;
		} catch (ResourceException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}  
		hotelResource.release();

		return "";
	}
	
	public String getReservations(int idUser){
 
		ClientResource hotelResource = new ClientResource(baseURL+"/book");  
		hotelResource.getReference().addQueryParameter("idUser", Integer.toString(idUser));
		
		try {
			
			String result =  hotelResource.get().getText();
			hotelResource.release();
			return result;
		} catch (ResourceException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}  
		return "";
	}
	
	public String bookHotel(String startDate,String endDate, int idUser, int idHotel, int idRoom){

		// Create the client resource  
		ClientResource bookingResource = new ClientResource(baseURL+"/book");  
 
		Form form = new Form();  
		form.add("startDate", startDate);  
		form.add("endDate", endDate);  
		form.add("idRoom", Integer.toString(idRoom));  
		form.add("idUser", Integer.toString(idUser));  
		form.add("idHotel", Integer.toString(idHotel));  

		try {
			String resultBooking = bookingResource.post(form).getText();
			bookingResource.release();
			
			return resultBooking;
			
		} catch (ResourceException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}  
		return "";
	}
	
	public String cancelBooking(int idReservation){

		// Create the client resource  
		ClientResource bookingResource = new ClientResource(baseURL+"/cancel");

		try {
			bookingResource.getReference().addQueryParameter("idReservation", String.valueOf(idReservation));
			String resultBooking = bookingResource.delete().getText();
			bookingResource.release();
			
			return resultBooking;

		} catch (ResourceException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}  
		
		return "";
	}
	
	public String connection(String mail, String pwd) {
		
		ClientResource connection = new ClientResource(baseURL+"/connection");  
 
		Form form = new Form();  
		form.add("email", mail);  
		form.add("pwd", pwd);

		try {
			String resultConnection = connection.post(form).getText();
			connection.release();
			
			return resultConnection;
			
		} catch (ResourceException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}  
		return "";
	}
	
	public String disconnection() {
		ClientResource disconnection = new ClientResource(baseURL+"/disconnection");  
		 
		
		try {
			String resultDisconnection = disconnection.get().getText();
			disconnection.release();
			
			return resultDisconnection;
			
		} catch (ResourceException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}  
		return "";
	}

}
