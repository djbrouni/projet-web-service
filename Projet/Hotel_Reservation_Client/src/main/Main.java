package main;

import view.Hotel_Client_View;

public class Main {

	public static void main(String[] args) {
		
		
		boolean stop = false;
		
		do {
			
			int choix = 99;
		
			System.out.println("Connexion :");
			System.out.println("Saisissez votre email");
			String email = Hotel_Client_View.scanner.nextLine();
			System.out.println("Saisissez votre mot de passe");
			String mdp = Hotel_Client_View.scanner.nextLine();
			
			String reponseConnection = Hotel_Client_View.client.connection(email, mdp);
			
			System.out.println(reponseConnection);
			
			if(reponseConnection.equals("La connexion a échoué, rééssayer ?(o/n)")) {
				String rep = Hotel_Client_View.scanner.nextLine();
				if(rep.equals("n")) {
					stop = true;
					Hotel_Client_View.scanner.close();
				}
				choix = -1;
			}
			
			while(choix > 0){
		    	Hotel_Client_View.menu();
		    	
		    	choix = Integer.parseInt(Hotel_Client_View.scanner.nextLine());
		    	
		    	switch(choix)
		        {
		            case(1) : 
		            	Hotel_Client_View.caseone();
	
		                break;
	
		            case(2) : 
		                Hotel_Client_View.casetwo();
	
		                break;
	
		            case(0) :
		                System.out.println("Voulez-vous vraiment partir ? (o/n)");
		                String ans = Hotel_Client_View.scanner.nextLine();
		                if(ans.equals("n")){
		                    choix=99;
		                }
		                else {
		                	Hotel_Client_View.scanner.close();
		                	stop = true;
		                }
	
		                break;
	
		        }
	
		    }
		    
		}while(!stop);
		
		System.out.println(Hotel_Client_View.client.disconnection());		
		
	}
	
}
