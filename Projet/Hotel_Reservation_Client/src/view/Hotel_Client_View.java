package view;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

import model.Hotel_Client_Model;

public class Hotel_Client_View {
	
	public static Hotel_Client_Model client = new Hotel_Client_Model();
	public static Scanner scanner = new Scanner(System.in);
	
	public static void menu() {

	    System.out.println("[1] - Voir les chambres disponibles sur une periode précise et possibilité de reserver une chambre");
	    System.out.println("[2] - Consulter ses reservations et possibilité d'annuler une reservation");
	    System.out.println("[0] - Quitter ?");
	    
	}
	
	public static void caseone() {

		

		
		boolean checkStartDate;
    	boolean checkEndDate;
    	boolean compareDate = true;
    	String startDate;
    	String endDate;
        do {
        	if(!compareDate) {
        		System.out.println("Votre date de début est superieure à votre date de fin.");
        	}
    		do {
        		System.out.println("Insérez la date de début (AAAA-MM-JJ) :");
            	
            	startDate = scanner.nextLine();
            	
            	checkStartDate = checkDate(startDate); 
            	
        	}while(!checkStartDate);
        	
        	do {
        		System.out.println("Insérez la date de fin (AAAA-MM-JJ) :");
        		
        		endDate = scanner.nextLine();
        		
        		checkEndDate = checkDate(endDate); 
        	}while(!checkEndDate);
        	
        	compareDate = compare(startDate,endDate);
        	
		}while(!compareDate);
        System.out.println("Voici la liste des chambres disponibles");

        System.out.println(client.getAvailableRooms(startDate,endDate));
        

        System.out.println("Voulez-vous réserver ? (o/n)");
        String rep = scanner.nextLine();
        
        if(rep.equals("o")) {
        	System.out.println("Donnez l'id de la chambre");
        	int idRoom = Integer.parseInt(scanner.nextLine());
        	
        	System.out.println("Donnez l'id de l'Hôtel");
        	int idHotel = Integer.parseInt(scanner.nextLine());
        	
        	System.out.println(client.bookHotel(idHotel, idRoom, startDate, endDate));
        }
        
	}
	
	
	public static void casetwo() {
		
		System.out.println("Voici la liste des réservations :");
		
        System.out.println(client.getReservation());
        
        System.out.println("Voulez-vous supprimer une réservation ? (o/n) ");
        
        String reponse = scanner.nextLine(); 
        
        if(reponse.equals("o")) {
        	System.out.println("Saisissez l'ID de la réservation à suprimer.");
        	
        	int idReservation = Integer.parseInt(scanner.nextLine());
        	
        	System.out.println(client.cancelBooking(idReservation));
        	
        }

	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	private static boolean checkDate(String strDate) {
		SimpleDateFormat sdfrmt = new SimpleDateFormat("yyyy-MM-dd");
	    sdfrmt.setLenient(false);
	    /* Create Date object
	     * parse the string into date 
	         */
	    try
	    {
	        sdfrmt.parse(strDate); 
	    }
	    /* Date format is invalid */
	    catch (ParseException | java.text.ParseException e)
	    {
	        System.out.println(strDate+" n'est pas au format valide.");
	        return false;
	    }
	    /* Return true if date format is valid */
	    return true;
	}
	
	private static boolean compare(String startDate,String endDate) {
		
		boolean isSuperior;
		SimpleDateFormat sdfrmt = new SimpleDateFormat("yyyy-MM-dd");
	    sdfrmt.setLenient(false);
	    /* Create Date object
	     * parse the string into date 
	         */
	    try
	    {
	     Date start = sdfrmt.parse(startDate);
	     Date end = sdfrmt.parse(endDate);
	     
	     isSuperior = start.compareTo(end) <= 0;
	     
	        
	    }
	    catch (ParseException | java.text.ParseException e)
	    {
	        System.out.println("L'une des dates n'est pas au format valide.");
	        return false;
	    }

		
		return isSuperior;
	}
	
}
