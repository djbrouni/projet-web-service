package model;

import java.rmi.RemoteException;

import org.apache.axis2.AxisFault;

import tps.ws.deployment.HotelSOAPStub;
import tps.ws.deployment.HotelSOAPStub.BookHotel;
import tps.ws.deployment.HotelSOAPStub.CancelBooking;
import tps.ws.deployment.HotelSOAPStub.Connection;
import tps.ws.deployment.HotelSOAPStub.Disconnection;
import tps.ws.deployment.HotelSOAPStub.GetAvailableRooms;
import tps.ws.deployment.HotelSOAPStub.GetReservations;

public class Hotel_Client_Model {
	
	private int idClient;
	
	public String getAvailableRooms(String startDate, String endDate){
		try {
			
			HotelSOAPStub stub = new HotelSOAPStub();
			
			GetAvailableRooms getrooms = new GetAvailableRooms();
			getrooms.setEndDate(endDate);
			getrooms.setStartDate(startDate);
			
			String result = stub.getAvailableRooms(getrooms).get_return();
			
			return result;
						
			
		} catch (AxisFault e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return "not found";
		
	}
	
	public String connection(String email, String pwd) {
		try {
			HotelSOAPStub stub = new HotelSOAPStub();
			
			Connection connection = new Connection();
			connection.setMail(email);
			connection.setPwd(pwd);
			
			String result = stub.connection(connection).get_return();
			
			
			if(!result.equals("-1")) {
				this.idClient = Integer.parseInt(result);
				
				return "Connexion Réussie";
			}	
			
		} catch (AxisFault e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return "La connexion a échoué, rééssayer ?(o/n)";
	}
	
	public String disconnection() {
		try {	
			HotelSOAPStub stub = new HotelSOAPStub();
			
			Disconnection disconnection = new Disconnection();
			
			String result = stub.disconnection(disconnection).get_return();
			
			return result;
			
		}
		catch (AxisFault e) {
			e.printStackTrace();
		} 
		catch (RemoteException e) {
			e.printStackTrace();
		}
		return "";

	}
	
	public String bookHotel(int idHotel, int idRoom, String startDate, String endDate) {
		try {
			HotelSOAPStub stub = new HotelSOAPStub();
			
			BookHotel bookHotel = new BookHotel();
			bookHotel.setIdUser(this.idClient);
			bookHotel.setEndDate(endDate);
			bookHotel.setIdHotel(idHotel);
			bookHotel.setIdRoom(idRoom);
			bookHotel.setStartDate(startDate);
			
			String result = stub.bookHotel(bookHotel).get_return();
			
			return result;
						
			
		} catch (AxisFault e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return "Reservation Error";
	}
	
	
	public String getReservation() {
		try {
			HotelSOAPStub stub = new HotelSOAPStub();
			
			GetReservations getReservations = new GetReservations();
			getReservations.setIdUser(this.idClient);
			
			String result = stub.getReservations(getReservations).get_return();
			
			return result;
						
			
		} catch (AxisFault e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return "";
	}
	
	public String cancelBooking(int idReservation) {
		try {
			HotelSOAPStub stub = new HotelSOAPStub();
			
			CancelBooking cancelBooking = new CancelBooking();
			cancelBooking.setIdReservation(idReservation);
			
			String result = stub.cancelBooking(cancelBooking).get_return();
			
			return result;
						
			
		} catch (AxisFault e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return "";
	}
	
	
	
}
