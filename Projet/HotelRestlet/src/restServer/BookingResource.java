package restServer;

import java.util.ArrayList;
import java.util.List;

import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import com.BDHotel.bdd.Hotel_JBDC;

public class BookingResource extends ServerResource {

	@Post
	public Representation bookHotel(Representation entity) {

		Form form = new Form(entity);
		int idUser = Integer.parseInt(form.getFirstValue("idUser"));
		int idHotel = Integer.parseInt(form.getFirstValue("idHotel"));
		int idRoom = Integer.parseInt(form.getFirstValue("idRoom"));
		String startDate = form.getFirstValue("startDate");
		String endDate = form.getFirstValue("endDate");
		List<String> resultat = new ArrayList<String>();
		resultat = Hotel_JBDC.executeQueryAddReservation(startDate, endDate, idUser, idRoom, idHotel);

		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < resultat.size(); i++) {
			sb.append(resultat.get(i));
		}

		return new StringRepresentation(sb.toString(), MediaType.TEXT_PLAIN);

	}
	
	@Get  
	public Representation getReservations(Representation entity) {
		
		List<String> resultat = new ArrayList<String>();
		
		int idUser = Integer.parseInt(getQuery().getValues("idUser"));
		
		resultat = Hotel_JBDC.executeQueryAllReservations(idUser);
		
		String result = "";
		
		for(int i = 0; i<resultat.size(); i++) {
			result += resultat.get(i);
		}
               
		return new StringRepresentation(result,MediaType.TEXT_PLAIN);
	       		
	}

}
