package restServer;

import java.util.ArrayList;
import java.util.List;

import com.BDHotel.bdd.*;


import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

public class HotelResource extends ServerResource  {

 	@Get  
	public Representation getAvailableRooms(Representation entity) {
		
		List<String> resultat = new ArrayList<String>();
		
		String stringStartDate = getQuery().getValues("startDate");
		String stringEndDate = getQuery().getValues("endDate");
		
		resultat = Hotel_JBDC.executeQueryAvailableRooms(stringStartDate, stringEndDate);
		
		String result = "";
		
		for(int i = 0; i<resultat.size(); i++) {
			result += resultat.get(i);
		}
               
		return new StringRepresentation(result,MediaType.TEXT_PLAIN);
	       		
	} 
	
}
