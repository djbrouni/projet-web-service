package restServer;

import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import com.BDHotel.bdd.Hotel_JBDC;

public class BDDisconnection extends ServerResource{
	
	@Get  
	public Representation disconnectionBD(Representation entity) {
        
		
		String result = "";
		
		result = Hotel_JBDC.disconnect();
               
		return new StringRepresentation(result,MediaType.TEXT_PLAIN);
	       		
	}
	
}
