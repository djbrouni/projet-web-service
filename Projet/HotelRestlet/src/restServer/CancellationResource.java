package restServer;

import java.util.ArrayList;
import java.util.List;

import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Delete;
import org.restlet.resource.ServerResource;

import com.BDHotel.bdd.Hotel_JBDC;

public class CancellationResource extends ServerResource {

	@Delete
	public Representation deleteReservation(Representation entity) {
		
		List<String> resultat = new ArrayList<String>();
		
		int idReservation = Integer.parseInt(getQuery().getValues("idReservation"));
		
		resultat = Hotel_JBDC.executeQueryDeleteReservation(idReservation);
		
		String result = "";
		
		for(int i = 0; i<resultat.size(); i++) {
			result += resultat.get(i);
		}
               
		return new StringRepresentation(result,MediaType.TEXT_PLAIN);
	       		
	}
	
}
