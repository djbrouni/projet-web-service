package restServer;

import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import com.BDHotel.bdd.Hotel_JBDC;

public class BDConnection extends ServerResource{
	
	@Post  
	public Representation connectionBD(Representation entity) {
		
		Hotel_JBDC.connect();
		
		
		Form form = new Form(entity);
		String email = form.getFirstValue("email");
		String password = form.getFirstValue("pwd");

		
		String result = Hotel_JBDC.executeQueryCheckUser(email, password);

		return new StringRepresentation(result, MediaType.TEXT_PLAIN);

	       		
	}
	
}
